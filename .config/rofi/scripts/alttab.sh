#!/bin/bash
theme="-theme themes/alttab.rasi"
filter="-filter -urxvt"
extra=""
if [[ $XDG_SESSION_DESKTOP == "openbox" ]]; then
    filter=""
    extra="-window-thumbnail"
fi

rofi $theme $filter -show window -modi window $extra
