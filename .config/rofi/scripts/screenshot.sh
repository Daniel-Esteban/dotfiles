#!/bin/bash

cmd="rofi -theme themes/screenshot.rasi -columns 3 -lines 2 -p Screenshot -dmenu"
fpath="$HOME/Pictures/Screenshots/"
fname="_maim_$(date +%s).png"
xclip="xclip -selection clipboard -t image/png"
feh="feh -g 800x600 -."

pcmd="rofi -dmenu -i -no-fixed-num-lines -p \"Elige Prefijo\""
f_b=""
s_b=""
f_c=" "
s_c=" "
f_f=" "
s_f=" "
menu="$f_b\n$s_b\n$f_c\n$s_c\n$f_f\n$s_f"
ch=$(echo -e "$menu" | $cmd)

time=0.15

fullpath="$fpath$fname"
prompt_name() {
    pf=$(rofi -dmenu -i -no-fixed-num-lines -p "Elige Prefijo")
    fullpath="$fpath$pf$fname"
    sleep $time
}

tmp="/tmp/screenshot.png"
notify_tmp(){
    xclip -selection clipboard -o > $tmp
    notify $tmp
}

notify(){
    x=`dunstify -i $1 -A "yes,ACCEPT" -t 5000 -a "System" "Screenshot" "$1"`
    [[ $x = 2 ]] && feh --title "Screenshot" $1
}

echo $ch
case $ch in
    $f_b)
        prompt_name
        maim | tee "$fullpath" | $xclip
        notify "$fullpath"
        ;;
    $s_b)
        prompt_name
        maim -suD | tee "$fullpath" | $xclip
        notify "$fullpath"
        ;;
    $f_c)
        sleep $time
        maim | $xclip
        notify_tmp
        ;;
    $s_c)
        maim -suD | $xclip
        notify_tmp
        ;;
    $f_f)
        prompt_name
        maim "$fullpath"
        notify "$fullpath"
        ;;
    $s_f)
        prompt_name
        maim -suD $fullpath
        notify "$fullpath"
        ;;
esac

