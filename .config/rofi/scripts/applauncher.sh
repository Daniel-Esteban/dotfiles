#!/bin/bash
theme="-theme themes/applauncher.rasi"
if [[ $XDG_SESSION_DESKTOP == "openbox" ]]; then
    filter=""
fi
rofi $theme $icon -show drun -modi drun,run -lines 5 -columns 4

