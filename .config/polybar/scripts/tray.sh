#!/bin/bash
pid=`pgrep -x trayer`

if [ $pid ]; then
    killall trayer
else
    trayer \
        --SetDockType false \
        --transparent true \
        --alpha 0 \
        --tint 0x2D2D2D \
        --expand false \
        --widthtype request \
        --distancefrom bottom \
        --distance -27 \
        --edge bottom \
        --align right \
        &
fi