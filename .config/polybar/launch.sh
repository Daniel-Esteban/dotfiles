#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

monitors=`polybar -m | cut -d ':' -f 1`
for m in $monitors; do
    MONITOR="$m" polybar $1 &
done
