if exists("g:loaded_tex_autoload")
    finish
endif
let g:loaded_tex_autoload = 1

fun! tex#Compile()
    exec 'silent !pdflatex %' | redraw!
endfun

fun! tex#Preview()
    exec 'silent !zathura %:r.pdf &'
endfun
