if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim'
fi

plugins=(git docker)



ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[blue]%}("
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%})%{$fg[red]%}⚡%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"

export PATH=$PATH:$GOPATH/bin:$HOME/bin
export GOPATH=$HOME/go
export MYVIMRC=$HOME/.vim/vimrc
export ZSH=$DOTFILES/submodules/.oh-my-zsh
export TEXINPUTS=.:$DOTFILES/texmf/tex/latex/:$TEXINPUTS

#PROMPT
export PROMPT=$'%F{cyan}%m $(git_prompt_info)%F{yellow}%~\n%F{green}%#%F{white} '

# source $ZSH/oh-my-zsh.sh
