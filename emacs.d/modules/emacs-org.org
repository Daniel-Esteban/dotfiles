#+TITLE: Emacs Config (ORG)
#+AUTHOR: Daniel Esteban

  #+BEGIN_SRC emacs-lisp
    (setq org-hide-leading-stars t)
    (setq org-hide-emphasis-markers t)

    (evil-leader/set-key-for-mode 'org-mode "o" 'org-open-at-point)
    (evil-leader/set-key-for-mode 'org-mode "i" 'org-toggle-inline-images)
    (evil-leader/set-key-for-mode 'org-mode "l" 'org-toggle-link-display)
  #+END_SRC

#+BEGIN_SRC emacs-lisp
  (setq org-file-apps '((auto-mode . emacs)
   ("\\.mm\\'" . default)
   ("\\.x?html?\\'" . default)
   ("\\.pdf\\'" . "zathura %s")))
#+END_SRC
* LaTeX
    Latex works with minted
#+BEGIN_SRC emacs-lisp
  (setq org-export-latex-listings 'minted)
  (add-to-list 'org-latex-packages-alist '("" "xcolor"))
  (add-to-list 'org-latex-packages-alist '("" "minted"))
  (setq org-latex-listings 'minted
        org-latex-pdf-process
        '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
  (setq org-latex-minted-options
        '(
          ("frame" "single")
          ("breaklines" "true")
          ("framesep" "10pt")
          ("linenos" "false")))
#+END_SRC

* Babel
#+BEGIN_SRC emacs-lisp
  (org-babel-do-load-languages 'org-babel-load-languages
                               '((shell . t)
                                 (dot . t)
                                 (plantuml . t)
                                 (ditaa . t)))
  (use-package ob-mermaid
    :ensure t
    :init
    (setq ob-mermaid-cli-path (expand-file-name "~/node_modules/.bin/mmdc" ))
    :config
    (add-to-list 'org-babel-load-languages
                 '(mermaid . t)))
#+END_SRC

