(defconst myemacsd (expand-file-name user-emacs-directory))
(org-babel-load-file (concat myemacsd "emacs.org"))
(setq custom-file (expand-file-name "custom.el" myemacsd))
