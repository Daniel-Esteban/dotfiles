# ssh
export SSH_KEY_PATH="~/.ssh/id_rsa"

for f in `find $DOTFILES/zsh`
do
   [[ -f "$f" ]] && source $f
done

plugins=(git docker)
source $ZSH/oh-my-zsh.sh

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[blue]%}("
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%})%{$fg[red]%}⚡%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"

export PATH=$PATH:$GOPATH/bin:$HOME/bin
export GOPATH=$HOME/go
export MYVIMRC=$HOME/.vim/vimrc
export ZSH=$DOTFILES/submodules/.oh-my-zsh
export TEXINPUTS=.:$DOTFILES/texmf/tex/latex/:$TEXINPUTS

#PROMPT
export PROMPT=$'%F{cyan}%m $(git_prompt_info)%F{yellow}%~\n%F{green}%#%F{white} '


alias em='emacsclient --socket-name=server --no-wait -c'
alias org='emacsclient --socket-name="eorg" --no-wait -c'
